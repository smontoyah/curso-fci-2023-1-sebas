# Purpose: class to solve the integral of a function using Monte Carlo method and analitic method
# Author:  Manolo Cardona M
# Date:    07/05/2023
#librerías
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import quad 

class integra():
    def __init__(self,f,n,a,b): # f: function, n: number of random points, a: initial point, b: final point
        self.f = f # function
        self.n = n # number of random points
        self.a = a # initial point
        self.b = b # final point
        self.max = max(self.f(np.linspace(self.a,self.b,self.n))) # maximum value of the function
        self.min = min(self.f(np.linspace(self.a,self.b,self.n))) # minimum value of the function
        self.x= np.random.uniform(self.a, self.b, size=(1, self.n)) # random points
        self.y= np.random.uniform(self.min, self.max, size=(1, self.n)) # random points

    def solve(self): # solve the equation
        self.interior = ((self.y <= self.f(self.x)) & (self.y >= 0)) | ((self.y >= self.f(self.x)) & (self.y <= 0)) # points inside the curve
        self.exterior = np.invert(self.interior) # points outside the curve
        p = self.interior.sum()/self.n # probability
        return p*(self.b-self.a)*(self.max-self.min) # integral
    
    def analytic(self): # solve the equation analytically
        I,e = quad(self.f, self.a, self.b) # integral
        return I
    
    def plot(self): # plot the solution
        plt.plot(self.x[self.exterior], self.y[self.exterior], 'bo')
        plt.plot(self.x[self.interior], self.y[self.interior], 'ro')
        plt.xlim(self.a,self.b)
        plt.show()



