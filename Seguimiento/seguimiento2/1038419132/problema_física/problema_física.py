
import random

class Termico: # Sistema termodinámico
    def __init__(self, N1, N2, T1, T2): # N1 y N2 son el número de partículas de cada subsistema
        self.N1 = N1
        self.N2 = N2
        self.T1 = T1
        self.T2 = T2
        self.energia1 = [T1] * N1
        self.energia2 = [T2] * N2
        self.t = 0

    def getTemperatura1(self): # Devuelve la temperatura del subsistema A
        return self.T1

    def getTemperatura2(self): # Devuelve la temperatura del subsistema B
        return self.T2

    def evolucion(self, dt): # Evoluciona el sistema durante dt unidades de tiempo
        for _ in range(dt):
            i = random.randint(0, self.N1 - 1)
            j = random.randint(0, self.N2 - 1)
            Et = self.energia1[i] + self.energia2[j]
            Ea = random.random() * Et
            Eb = Et - Ea
            self.T1 += (Ea - self.energia1[i]) / self.N1
            self.T2 += (Eb - self.energia2[j]) / self.N2
            self.energia1[i] = Ea
            self.energia2[j] = Eb
        self.t += dt




