# Punto 2: Distribución de velocidades de Maxwell-Boltzmann. Clase.
# Muy análogo al primer punto, sólo busco resolver la integral.

# Importo librerías.
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

# Defino clase.
class maxwellBoltzmann():
  # Método constructor.
  def __init__(self, a, b, N, m, r, T, P): # Se definen los atributos.
    self.a = a # Límite inferior de la integral.
    self.b = b # Límite superior de la integral.
    self.N = int(N) # Número de iteraciones, enteras.
    self.m = m # Masa del aire [kg].
    self.r = r # Radio de molécula de aire [m].
    self.T = T # Temperatura absoluta del sistema [K].
    self.P = P # Presión del sistema [Pa].
    self.ite = np.arange(1, N, 1) # Arreglo para iteraciones.
    self.Kb = 1.380649e-23 # Constante de Boltzmann.
    # Distribución de Maxwell-Boltzmann, media.
    self.alpha = self.m/(2*self.Kb*self.T)
    self.f = lambda v: v*(4*np.pi*((self.alpha/np.pi)**(3/2))*(v**2)*\
                        np.exp(-self.alpha*(v**2)))
    
    # Camino libre medio, media. El 1e10 se agrega por propósitos 
    # de visibilización de datos (al ser tan pequeños), luego basta con dividir.
    # El valor no dará exacto pero se aproximará.
    self.lambdap = ((self.Kb*self.T)/(4*np.pi*np.sqrt(2)*(self.r**2)*self.P))*1e10
    self.fp = lambda l: l*((1/self.lambdap)*np.exp(-l/self.lambdap))

  def metExacto(self): # Método para solución analítica.
    sol1 = integrate.quad(self.f, self.a, self.b) # Función scipy.
    sol2 = integrate.quad(self.fp, self.a, self.b)
    sol = np.array([np.round(sol1, 5), sol2])
    return sol
  
  def monteCarlo(self):
    s = 0
    sp = 0
    n = int(10e3)
    for i in range(n):
      xrand = np.random.uniform(self.a, self.b)
      s += self.f(xrand)
      sp += self.fp(xrand)
      sol1 = (self.b - self.a)*(s/n)
      sol2 = (self.b - self.a)*(sp/n)
    sol = np.array([np.round(sol1, 5), sol2])
    return sol

  def grafVelMed(self):
    fig = plt.figure(figsize=(12,8))
    solana = self.metExacto()[0,0]
    solmonte = []
    for i in self.ite:
        solmonte.append(self.monteCarlo()[0])

    plt.plot(self.ite, solmonte, color='blue', linestyle='-', label='Solución con Monte Carlo')
    plt.axhline(solana, color='black', linestyle='-',label='Solución analítica')
    plt.xlabel("Iteraciones")
    plt.ylabel("Velocidad media")
    plt.title("Velocidad media de moléculas de aire")
    plt.legend()
    plt.grid()
    plt.savefig("grafMaxwBoltzP2.png")
    plt.show()

  def grafCaminoLibre(self):
    fig = plt.figure(figsize=(12,8))
    solana = self.metExacto()[1,0]
    solmonte = []
    for i in self.ite:
        solmonte.append(self.monteCarlo()[1])

    plt.plot(self.ite, solmonte, color='blue', linestyle='-', label='Solución con Monte Carlo')
    plt.axhline(solana, color='black', linestyle='-',label='Solución analítica')
    plt.xlabel("Iteraciones")
    plt.ylabel("Camino libre medio")
    plt.title("Camino libre medio de moléculas de aire")
    plt.legend()
    plt.grid()
    plt.savefig("grafCamLibMedP2.png")
    plt.show()