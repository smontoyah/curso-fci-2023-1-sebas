# aca haremos una simulacion del movimiento browniano en una dimension
# tendremos en cuenta todos los conceptos teoricos necesarios para tener una buena simulacion
# supongamos que tenemos un particula de un grano de polen en un fluido esta empezara a tener un movimiento aleatorio
# queremos solucionar este problema con montecarlo
# debemos de aplicar un ruido gaussiano debido a que la particula tendra este tipo de distribucion
import numpy as np
import matplotlib.pyplot as plt
class browniano():
    def __init__(self,N,dt,media,desviacion):
        self.N=N  # numero de puntos
        self.dt=dt # incremento del tiepo
        self.media=media # media
        self.desviacion=desviacion # desviacion estandard
    def movimiento(self):
        lst = [0] * 1000

        for i in range(1, self.N):      
            dx = np.random.normal(self.media, self.desviacion) * np.sqrt(self.dt)
            lst[i] = lst[i-1] + dx     
        return lst   # aca tenemos la posicion en x basta completar la posicion en y.
    def intervalo_time(self): # el intervalo nos mostrara las posiciones del tiempo
        t=np.arange(self.N)*self.dt # dt es el paso a considerar por cada interacion
        return t
    def grafica(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.intervalo_time(),self.movimiento())
        plt.savefig("browniano1.png")
        plt.xlabel("tiempo")
        plt.ylabel("posicion particula")
        plt.grid()
        plt.show()
    

    
    
        
        