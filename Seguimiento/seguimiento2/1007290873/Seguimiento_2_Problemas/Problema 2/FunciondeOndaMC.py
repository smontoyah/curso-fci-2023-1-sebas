import numpy as np
import matplotlib.pyplot as plt
import cmath

class funciondeOndaMC():

    """
    Clase que permite resolver el problema de una partícula atrapada en un pozo de potencial infinito de ancho L,
    consideramos el problema dependiente del tiempo, donde la condición inicial sobre la función de onda
    en la rep. posiciones es dada, tiene una forma triangular: entre 0 y L/2, es 2sqrt(3/L^3)*x; y entre
    L/2 y L, es 2sqrt(3/L^3)*(L - x).

    De acuerdo con la solución analítica los coeficiente pares (C2, C4, C6, ...), que multiplican a los autoestados
    y la correspondiente fase que depende de la energía, son iguales a 0

    En la aproximación que resolveremos, consideraremos los tres primeros términos, es decir los correspondientes a
    C1, C3 y C5 (tenemos expresiones halladas analíticamente). Con MonteCarlo resolveremos las integrales que definen
    estos coeficientes.   

    Asumimos un ancho del pozo contante igual L = 1.
    """
    
    # Atributos de clase:

    # Longitud del pozo
    L = 1 # [m]
    # Arreglo con los posibles valores de n que vamos a considerar en la aproximación (solo los impares son distintos de 0)
    n_array = np.array([1, 3, 5]) 
    # Valor de h barra
    h_bar = 1.0545718173e-34 # [Js]
    # Valores de los coeficientes en la expansión de la función de onda en términos de los autoestados, para n = 1,3,5
    C_analytical = ((4 * np.sqrt(6)) / np.pi**2) * np.array([1, -1/9, 1/25])

    # Constructor

    def __init__(self, MC_points, particle_mass, time):

        """
        Parámetros con los que se instancia la clase

        MC_points: número de puntos que se usaran en la integración con MC (entero) 
        particle_mass: masa de la partícula atrapada en el pozo de potencial
        time: tiempo en el cual se quiere evaluar la función de onda (> 0)
        """

        # Definición de atributos de instancia

        self.N = MC_points
        self.m = particle_mass
        self.t = time

    # Definición de métodos

    # Función que corresponde al autoestado n en la rep. posiciones
    def n_eigenstate(self, n, x):
        return np.sqrt(2 / self.L) * np.sin((n * np.pi * x) / self.L)

    # Función de la energía correspondiente al n-ésimo autoestado
    def n_Energy(self, n): 
        return (n * np.pi * self.h_bar)**2 / (2 * self.m * self.L**2)


    # Definimos las dos funciones apartir de las cuales calculamos las integrales de los coeficientes

    # Función para el intervalo [0, L/2]
    def function_first_interval(self, n, x):
        return x * np.sin((n * np.pi * x) / self.L)

    # Función para el intervalo [L/2, L]
    def function_second_interval(self, n, x):
        return (self.L - x) * np.sin((n * np.pi * x) / self.L)

    
    def calculate_MC_area(self, positive, a, b, height, n, function):
        
        """
        Método para calcular el valor del área en un cuadrado dado
        - positive es un bool que indica si en la región de interés la función es positiva o negativa
          Positiva -> positive = True, Negativa -> positive = False
        - (a, b) es el intervalo en x donde se define el cuadrado
        - height es la altura del cuadrado, es el valor máximo o mínimo que alcanza la función, dependiendo
          de si e suna región positiv o negativa 
        - n es el entero que define el autoestado y el autovalor de energía
        - function es la función que se está integrando, puede ser function_first_interval o function_second_interval
          dependiendo de en qué intervalo se encuentre (a, b)
        """

        # Se fijia una semilla
        np.random.seed(2)

        # Se genera el arreglo aleatorio para el eje x
        x_random_array = np.random.uniform(a, b, size = self.N)

        # Se evalua la anterior función en este arreglo
        function_evaluated_random  = function(n, x_random_array)

        if positive:

            # Caso en que la función es positiva en (a, b)

            # Se genera el arreglo aleatorio para el eje y
            y_random_array = np.random.uniform(0, height, size = self.N)
            
            # Máscara booleana de la verificación de que los puntos estén dentro de la curva de la función 
            inside_bool_mask = y_random_array <= function_evaluated_random

        else:

            # Caso en que la función es negativa en (a, b)

            # Se genera el arreglo aleatorio para el eje y
            y_random_array = np.random.uniform(height, 0, size = self.N)

            # Máscara booleana de la verificación de que los puntos estén dentro de la curva de la función 
            inside_bool_mask = function_evaluated_random <= y_random_array
        

        # Se cuentan el número de puntos dentro de la curva
        points_inside = np.sum(inside_bool_mask)

        # Se obtiene la probabilidad, dividiendo por el número de puntos
        probability = points_inside / self.N

        # Se obtiene el área del cuadrado
        square_area = height * (b - a)

        # Se multiplica la probabilidad por el área del cuadrado, para obtener 
        # el área por debajo de la curva
        return probability * square_area

    # Método que permiten calcular los valores de los coeficientes que acompañan las autofunciones en la sumatoria
    # de la función de onda

    def C1_MC_value(self):

        # Primer coeficiente C1
        # Se generan solo dos cuadrados de ptos aleatorios, uno por intervalo

        # Altura que tienen los dos cuadrado  
        height = self.function_first_interval(self.n_array[0], self.L/2)

        # Cálculo de la integral del primer intervalo
        C1_first_interval_integral = self.calculate_MC_area(True, 0, self.L/2, height, self.n_array[0], self.function_first_interval)

        # Cálculo de la integral del segundo intervalo
        C1_second_interval_integral = self.calculate_MC_area(True, self.L/2, self.L, height, self.n_array[0], self.function_second_interval)

        # Se suman para obtener el valor total de C1
        C1_MC = ( (2 * np.sqrt(6)) / (self.L**2) ) * (C1_first_interval_integral + C1_second_interval_integral)

        return C1_MC

    def C3_MC_value(self):

        # Tercer coeficiente C3
        # Se generan cuatro cuadros de ptos aleatorios, dos por intervalo

        # Altura que tienen los cuadrados 
        # Los valores de x de los máximos se hallaron a partir de la gráfica de la función
        height_first_interval = self.function_first_interval(self.n_array[1], 0.21526)

        height_mid = self.function_first_interval(self.n_array[1], self.L/2)

        height_second_interval = self.function_second_interval(self.n_array[1], 0.78474)

        # Cálculo de las integrales del primer intervalo
        C3_first_interval_integral1 = self.calculate_MC_area(True, 0, 1/3, height_first_interval, self.n_array[1], self.function_first_interval)
        C3_first_interval_integral2 = self.calculate_MC_area(False, 1/3, self.L/2, height_mid, self.n_array[1], self.function_first_interval)

        # Cálculo de las integrales del segundo intervalo
        C3_second_interval_integral1 = self.calculate_MC_area(False, self.L/2, 2/3, height_mid, self.n_array[1], self.function_second_interval)
        C3_second_interval_integral2 = self.calculate_MC_area(True, 2/3, self.L, height_second_interval, self.n_array[1], self.function_second_interval)

        # Se suman para obtener el valor total de C3
        C3_MC = ( (2 * np.sqrt(6)) / (self.L**2) ) * ( C3_first_interval_integral1 + C3_first_interval_integral2 + C3_second_interval_integral1 + C3_second_interval_integral2)

        return C3_MC

    def C5_MC_value(self):
        
        # Quinto coeficiente C5
        # Se generan seis cuadros de ptos aleatorios, tres por intervalo

        # Altura que tienen los cuadrados 
        # Los valores de x de los máximos se hallaron a partir de la gráfica de la función
        height_first_interval1 = self.function_first_interval(self.n_array[2], 0.12915)
        height_first_interval2 = self.function_first_interval(self.n_array[2], 0.31278)
        
        height_mid = self.function_first_interval(self.n_array[2], self.L/2)

        height_second_interval1 = self.function_second_interval(self.n_array[2], 0.68722)
        height_second_interval2 = self.function_second_interval(self.n_array[2], 0.87084)

        # Cálculo de las integrales del primer intervalo
        C5_first_interval_integral1 = self.calculate_MC_area(True, 0, 1/5, height_first_interval1, self.n_array[2], self.function_first_interval)
        C5_first_interval_integral2 = self.calculate_MC_area(False, 1/5, 2/5, height_first_interval2, self.n_array[2], self.function_first_interval)
        C5_first_interval_integral3 = self.calculate_MC_area(True, 2/5, self.L/2, height_mid, self.n_array[2], self.function_first_interval)

        # Cálculo de las integrales del segundo intervalo
        C5_second_interval_integral1 = self.calculate_MC_area(True, self.L/2, 3/5, height_mid, self.n_array[2], self.function_second_interval)
        C5_second_interval_integral2 = self.calculate_MC_area(False, 3/5, 4/5, height_second_interval1, self.n_array[2], self.function_second_interval)
        C5_second_interval_integral3 = self.calculate_MC_area(True, 4/5, self.L, height_second_interval2, self.n_array[2], self.function_second_interval)

        # Se suman para obtener el valor total de C5
        C5_MC = ( (2 * np.sqrt(6)) / (self.L**2) ) * ( C5_first_interval_integral1 + C5_first_interval_integral2 + C5_first_interval_integral3 + C5_second_interval_integral1 + C5_second_interval_integral2 + C5_second_interval_integral3)

        return C5_MC

    # Función de onda obtenida para la aproximación de los 3 primeros términos, con los coeficientes obtenidos con MC 
    def wave_function_MC(self, x):
        return ( self.C1_MC_value() * self.n_eigenstate(self.n_array[0], x) * cmath.exp(-((self.n_Energy(self.n_array[0]) * self.t) / self.h_bar)*1j) 
            + self.C3_MC_value() * self.n_eigenstate(self.n_array[1], x) * cmath.exp(-((self.n_Energy(self.n_array[1]) * self.t) / self.h_bar)*1j) 
            + self.C5_MC_value() * self.n_eigenstate(self.n_array[2], x) * cmath.exp(-((self.n_Energy(self.n_array[2]) * self.t) / self.h_bar)*1j) )

    # Función de onda obtenida para la aproximación de los 3 primeros términos, con los coeficientes de la solución analítica
    def wave_function_analytical(self, x):
        return ( self.C_analytical[0] * self.n_eigenstate(self.n_array[0], x) * cmath.exp(-((self.n_Energy(self.n_array[0]) * self.t) / self.h_bar)*1j) 
            + self.C_analytical[1] * self.n_eigenstate(self.n_array[1], x) * cmath.exp(-((self.n_Energy(self.n_array[1]) * self.t) / self.h_bar)*1j) 
            + self.C_analytical[2] * self.n_eigenstate(self.n_array[2], x) * cmath.exp(-((self.n_Energy(self.n_array[2]) * self.t) / self.h_bar)*1j) )
    

    def graph_wave_function_comparison(self):
        
        # Método para hacer una gráfica de comparación de los dos resultados

        plt.figure(figsize=(13,11))

        x = np.linspace(0, self.L, 200) 
        t_str = str(self.t)
        N_str = str(self.N)

        # Plot de la solución analítica
        plt.plot(x, self.wave_function_analytical(x).real, color='crimson', ls = 'dashed', label = r'Analítica Re $\Psi(x)$')
        plt.plot(x, self.wave_function_analytical(x).imag, color='navy', ls = 'dashed', label = r'Analítica Im $\Psi(x)$')

        # Plot de la solución con MC
        plt.plot(x, self.wave_function_MC(x).real, color='forestgreen', label = r'MC Re $\Psi(x)$')
        plt.plot(x, self.wave_function_MC(x).imag, color='darkviolet', label = r'MC Im $\Psi(x)$')

        plt.xlim(0, self.L)
        plt.title(r'Gráfica de la función de onda $\Psi$ en t =' + t_str + 's, MC generado con N ='+ N_str, fontsize=20)
        plt.xlabel("Posición $x$ (m)",fontsize=20)
        plt.ylabel("Función de onda $\Psi(x)$",fontsize=20)
        plt.grid()
        plt.xticks(size='14')
        plt.yticks(size='14')
        plt.legend(title = 'Solución', loc=1, prop={'size': 15})
        plt.savefig("Resultados_Funcion_de_Onda_Pozo_Potencial.png")
        

    
