import random
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import quad

class MonteCarloIntegrator:
    def __init__(self, f, a, b, num_samples): #constructor de la clase
        self.f = f #ingresamos los atributos de la clase
        self.a = a
        self.b = b
        self.num_samples = num_samples

    #creamos los metodos de la clase    
    def integrate(self): #integracion basica por Montecarlo
        integral_sum = 0
        
        for i in range(self.num_samples):
            x = random.uniform(self.a, self.b)
            integral_sum += self.f(x)
            
        integral = (self.b - self.a) * (integral_sum / self.num_samples)
        
        return integral
    
    def integral_exacta(self): #integracion con libreria standard
        integral, _ = quad(self.f, self.a, self.b)
        
        return integral
    
    def compare_methods(self): #metodo que imprime por consola los valores y su comapracion
        exact_integral = self.integral_exacta()
        mc_integral = self.integrate()
        error_relativo = abs((mc_integral - exact_integral) / exact_integral)
    
        print("La integral de la funcion entre {} y {} es:".format(self.a,self.b))
        print("Exacta (scipy):", self.integral_exacta())
        print("por Monte Carlo:", self.integrate())
        print("Error relativo de Montecarlo de {} %".format(error_relativo*100))

    def plot(self): #graficamos porque el punto lo pide
        exact_integral = self.integral_exacta()
        mc_integral = self.integrate()
        
        plt.bar(["Exacta", "Monte Carlo"], [exact_integral, mc_integral])
        plt.ylabel("Valor de la integral")
        plt.savefig("compacion_metodos.png")
        plt.show()


    

            

        
    
    