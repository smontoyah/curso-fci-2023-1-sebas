import random
import matplotlib.pyplot as plt

class Particle:
    def __init__(self, energy):
        self.energy = energy
        
    def lose_energy(self, delta_energy):
        self.energy -= delta_energy
        
class Material:
    def __init__(self, density, energy_loss_probabilities):
        self.density = density
        self.energy_loss_probabilities = energy_loss_probabilities
        
    def get_energy_loss(self):
    # Obtener una pérdida de energía aleatoria según las probabilidades definidas
     return random.choices(list(self.energy_loss_probabilities.keys()), 
                           list(self.energy_loss_probabilities.values()))[0]
    
    def get_energy_loss_probability(self, delta_energy):
    # Obtener la probabilidad de pérdida de energía para un valor de delta_energy dado
     return self.energy_loss_probabilities.get(delta_energy, 0)
    

        
# Esta función simula la evolución de la energía de una partícula que atraviesa un material,
# compuesto por varias células. Recibe como parámetros la partícula, el material y el número de células.

def simulate_particle(particle, material, num_cells):
    energies = [] # lista vacía para almacenar las energías de la partícula
    for i in range(num_cells): # para cada célula
        delta_energy = material.get_energy_loss() # se determina la energía que la partícula pierde en la célula
        probability = material.get_energy_loss_probability(delta_energy) # se obtiene la probabilidad de perder esa energía
        if random.uniform(0, 1) < probability: # si la partícula pierde esa energía con esa probabilidad
            particle.lose_energy(delta_energy) # se reduce la energía de la partícula
        energies.append(particle.energy) # se agrega la energía actual de la partícula a la lista
    return energies # devuelve la lista de energías de la partícula a lo largo del material



