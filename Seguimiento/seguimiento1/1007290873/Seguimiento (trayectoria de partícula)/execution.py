from LorentzForce import lorentzForce


"""
NOTA
5.0
MUY BUEN TRABAJO!

"""

if __name__ == "__main__":

    # Definimos los parámetros para instanciar la clase

    # Asumiendo que la partícula es un electrón
    e = 1.6e-19 # [C]
    B = 0#600e-6 # [T]
    m = 9.11e-31 # [kg]
    E_k = 18.6 # [eV]
    theta = 30 # [grados]

    particle_on_Lorentz_force = lorentzForce(e, B, m, E_k, theta)

    # Impresión de algunos atributos:

    print("Magnitud del campo magnetico: {} T".format(particle_on_Lorentz_force.B)) 

    print("Energía cinética de la partícula: {} J".format(particle_on_Lorentz_force.E_k))

    print("Magnitud de la velocidad: {} m/s".format(particle_on_Lorentz_force.v0))

    print("Frecuencia angular del movimiento: {} rad/s".format(particle_on_Lorentz_force.w))

    # Gráfica del movimiento

    particle_on_Lorentz_force.graph_trajectory()