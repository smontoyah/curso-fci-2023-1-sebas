from pendulo_balistico import PenduloBalistico
from pendulo_balistico import PenduloBalistico2
import numpy as np

if __name__=="__main__":

    #Para la configuración ingrese valores positivos
    m=0.2           # masa de la bala en kg
    M=1.5           # masa del bloque en kg
    R=0#0.5           # longitud del péndulo en m
    theta=45           # ángulo en grados entre 0 y 180
    g=9.8           # gravedad en m/s**2
    t=10            # tiempo final para la grafica de oscilaciones


    bala1=PenduloBalistico(m,M,R,theta,g,t)
    
    print(f"La velocidad inicial de la bala es {np.round(bala1.VelBala(),2)} m/s.")
    bala1.Graph()

    u0=10
    bala2=PenduloBalistico2(m,M,R,g,t,u0,theta)
    print(f"\nLa bala debe tener una velocidad inicial de {np.round(bala2.VelMinBala(),2)} m/s para que el péndulo de una vuelta completa.")
    print(f"La velocidad inicial del sistema bala bloque es {np.round(bala2.VelSis(),2)} m/s")