from pendulo_balistico import *
class pendbalisdis(pendbalis):
    """
    En esta clase agregaremos un factor de perdida de transmision de momento a la hora de quedar la bala
    incrustada. Esta representa perdida por deformacion o en forma de calor. El funcionamiento es analogo
    a la clase anterior, solo que ahora el factor de perdida puede ser variable. Esta disipacion esta 
    incluido en todos los metodos.

    Los parametros necesarios para definir la clase son:    
    - Masa del proyectil: m (Real positivo en SI)
    - Masa del bloque: M (Real positivo en SI)
    - Longitud de la cuerda: R (Real positivo en SI)
    - Aceleracion gravitacional: g (Real positivo en SI)
    - Factor de disipacion: alpha (Real positivo entre 0 y 1)
    
    """
    def __init__(self, m=0. , M= 0. , R= 0. , g= 0. , alpha= 1.):
        super().__init__(m, M, R, g)
        self.disip = alpha

    # Velocidad minima del proyectil para que el bloque comience a rotar:
    def vminrot(self):
        return 1/self.disip * 2*(1 + self.massbull/self.massblock)*np.sqrt(self.gravity * self.radius)
    
    # Angulo maximo alcanzado por el bloque dada una velocidad de proyectil:
    def maxangle(self, v):
        return super().maxangle(self.disip * v)
    
    # Velocidad de proyectil dado un angulo maximo alcanzado por el bloque:
    def velocity(self, theta):
        return 1/self.disip * super().velocity(theta)
    