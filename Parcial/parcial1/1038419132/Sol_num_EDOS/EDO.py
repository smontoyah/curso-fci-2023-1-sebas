import numpy as np
import sympy as sp
import matplotlib.pyplot as plt

#la primera clase es para resolver ecuaciones diferenciales de primer orden con condiciones iniciales por euler y analiticamente
#los parametros de entrada son: intervalo de integracion [a,b], numero de pasos n, condicion inicial y0, funcion a integrar f

#la segunda clase es para resolver la ecuacion diferencial del pendulo simple entonces se modifica pues es edo de segundo orden
#los parametros de entrada son: intervalo de integracion [a,b], numero de pasos n, condiciones iniciales y0 (matrix 2x1), funcion a integrar f,
#la frecuencia natural de oscilacion y el paso h que será un dt


class solnumerica(): #clase para resolver ecuaciones diferenciales de primer orden con condiciones iniciales por euler y analiticamente

    def __init__(self,a,b,n,y0,f):
        self.a = a #intervalo de integracion [a,b]
        self.b = b
        self.n = n #numero de pasos
        self.y0 = y0 #condicion inicial
        self.x = [] #vector de valores de x
        self.y = [] #vector de valores de y
        self.f = f #funcion a integrar

    def h(self): #paso de integracion
        return (self.b-self.a)/self.n


    def Euler(self): #metodo de Euler
        self.x = np.arange(self.a,self.b+self.h(),self.h()) #vector de valores de x espaaciados de h, el limite exterior se le agrega un paso h
                                                            #para que el ultimo valor de x sea b
        self.y = [self.y0] #vector de valores de y solo con la condicion inicial

        for i in range(self.n): #aplico el metodo de Euler para llenar el array y
            self.y.append(self.y[i] + self.h()*self.f(self.x[i]))

        return self.x,self.y #devuelvo los valores de x,y
    
    def analytic(self): # solve the equation analytically
        x = sp.symbols('x') #defino x como simbolo
        y = sp.Function('y') #defino y como funcion
        ED = sp.Eq(y(x).diff(x),self.f(x)) #defino la ecuacion diferencial
        CI = {y(self.a):self.y0} #defino la condicion inicial
        sl = sp.dsolve(ED,y(x),ics=CI) #solution
        fsol = sp.lambdify(x,sl.rhs,'numpy') #convierto la solucion en una funcion de numpy
        return fsol #devuelvo la funcion analitica de la solucion en uan funcion de numpy
    
    def plot(self): # plot the solution
        plt.plot(self.Euler()[0],self.Euler()[1],'r',label='Sol_Numérica para n = {}'.format(self.n)) #grafico la solucion numerica
        fsol = self.analytic() #obtengo la solucion analitica
        plt.plot(self.Euler()[0],fsol(self.Euler()[0]),'b',label='Sol_Analítica') #grafico la solucion analitica
        plt.title('Solución numérica y analítica de la EDO')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.xlim(self.a,self.b)
        #plt.ylim(-1,1)
        plt.legend()
        plt.savefig('Solucion_EDO.png')
        plt.show() #muestro el grafico
        #print(self.Euler()[1],self.analytic()(self.Euler()[0])) #imprimo los valores de y para compararlos

class PenduloSimple(solnumerica):
    def __init__(self, a, b, n, y0, f, w0,h): #y0 debe ser un array con las condiciones iniciales [theta0,omega0]
        super().__init__(a, b, n, y0, f) #a y b son el intervalo de integracion.
        self.w0 = w0 #w0 es la frecuencia angular de la oscilacion del pendulo
        self.h = h

    def Euler(self): #modificamos método de euler para el sistema de ecuaciones diferenciales
        u = self.y0 #condiciones iniciales
        def F(u,t):
            return np.array([u[1],-self.w0**2*u[0]]) #defino la funcion F
        
        self.tsol = [self.a] #vector de valores de t donde guardaré el tiempo de oscilacion
        t = self.a #valor inicial de t
        self.thetasol = [u[0]] #vector de valores de theta donde guardaré los valores de theta
        self.omegasol = [u[1]] #vector de valores de omega donde guardaré los valores de omega
        dt = self.h #paso de integracion
        tfin = self.b #tiempo final de integracion

        while t<tfin: #aplico el metodo de Euler para llenar los arrays tsol,thetasol,omegasol
            u = u + self.h*F(u,t) #aplico el metodo de Euler
            t = t + dt #avanzo el tiempo
            self.tsol.append(t) #guardo el tiempo
            self.thetasol.append(u[0]) #guardo el valor de theta
            self.omegasol.append(u[1]) #guardo el valor de omega
        return self.tsol,self.thetasol,self.omegasol #devuelvo los valores de tsol,thetasol,omegasol
    
    def analytic(self): # solve the equation analytically
        x = sp.symbols('x') #defino x como simbolo
        y = sp.Function('y') #defino y como funcion
        ED = sp.Eq(y(x).diff(x,2) - self.f(y(x)),0) #defino la ecuacion diferencial de segundo grado
        CI = {y(self.a):self.y0[0], y(x).diff().subs(x, self.a):self.y0[1]} #defino las condiciones iniciales
        sl = sp.dsolve(ED,y(x),ics=CI) #solution
        fsol = sp.lambdify(x,sl.rhs,'numpy') #convierto la solucion en una funcion de numpy
        return fsol #devuelvo la funcion analitica de la solucion en uan funcion de numpy
    
    def plot(self): # plot the solution
        plt.plot(self.Euler()[0], self.Euler()[1],'r',label='Sol_Numérica para h = {}'.format(self.h)) #grafico la solucion numericamente
        fsol = self.analytic() #obtengo la solucion analitica
        ll = np.array(self.Euler()[0]) 
        plt.plot(self.Euler()[0], fsol(ll), 'b', label='Sol_Analítica') #grafico la solucion analitica
        plt.title('Solución numérica y analítica de la EDO para un Pendulo Simple') #titulo del grafico
        plt.xlabel('t') #etiqueta del eje x
        plt.ylabel('theta')
        plt.xlim(self.a,self.b)
        plt.legend()
        plt.savefig('PenduloSimple.png')
        plt.show()
        

        

